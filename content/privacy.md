---
title: Privacy Policy
---

We will keep google forms and contact details safe, and only use them as needed for communications specifically about issues relating to the Festival we think you will want to hear about.

Let us know at any time if you'd like your data removed from our list.

hello *-at-* festival-of-commoning *-dot-* org