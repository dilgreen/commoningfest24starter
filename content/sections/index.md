---
title: Festival of the Commons 2024
headless: true
resources:
    - name: about
      src: about.md
    - name: details
      src: details.md
    - name: respond
      src: respond.md
    - src: 'features/*'
---

> this folder stores the content for the HOME page
