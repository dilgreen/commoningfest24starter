---
title: Seeding a Festival of Commoning
---

Launching a new Festival is not something obvious to do. And certainly not something to do if you are busy working on serious projects already.

But the energy in Stroud, where 'Commons' as an idea that brings people together around bringing the basics of local provisioning into community control only kicked off two years ago --- and the connections made with other towns --- and the number of projects that are popping up all over with 'commons' in the name or in the purpose --- is just remarkable.

This is not a festival just to ride on the back of that energy --- if it works at all, it is a festival AS a Commons --- all those who contribute, all those who participate, will be the Commoners of this Festival. This is intended to support us all and bring in new energy - perhaps even birth a movement!

***WHAT THIS MEANS IS THAT THE FESTIVAL WILL BE AS BIG AS EARLY CROWDFUNDING MAKES IT***

The guiding vision of those few of us who have got it this far is humble --- this year, we just want to successfully bring as many different Commons-interested people together, over a few days; to enable conversations, experience sharing, build connections, have a good time - and see what happens!

There's too much detail about how we want to take this forward to put on this makeshift website. You can read more ---- and comment on --- a longer document.

