---
title: Help the Festival become what it needs to be
---

We'd be grateful if you could spend ten minutes on a survey form, so we can understand what people hope for from the Festival, and what they would like to bring to it. 

In particular, and very soon, we need to understand how much crowdfunding support there is, and the survey has questions on that (no commitments asked for!).





