---
title: Exhibition / stalls
weight: 4
icon: stalls.webp
---

Make a showing! So many initiatives across the country are doing amazing and uplifting work (not to mention the hard graft..) to bring commoning to life and back to the forefront of our culture. Let people understand just what is already being done, inspire them to more!