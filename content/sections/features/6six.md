---
title: Have some fun!
weight: 6
icon: havefun.webp
---

Commons entertainment is most special when it's participatory and open to all - at least one Ceilidh. But we can look forward to choral participation, drumming, street dance and more.