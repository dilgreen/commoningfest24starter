---
title: Mini Conference
weight: 2
icon: conference.webp
---

Bring together practitioners, activists, workers and commoners with a sprinkling of experts and academics. Seed a Knowledge Commons of Commoning.