---
title: Make a noise!
weight: 5
icon: makeanoise.webp
---

Put Commoning on the news agenda, seed a movement for Commons infrastructure projects everywhere!