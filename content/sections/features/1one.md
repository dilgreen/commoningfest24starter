---
title: Common / Open Spaces
weight: 1
icon: commonspace.webp
---

Events, workshops, stalls open to everyone - no ticket required. Help people feed their interest --- build their confidence and understanding.