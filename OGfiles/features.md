---
title: What could the Festival have?
afterTitle: ...so much is possible - what can we achieve?
---

Right now, we're imagining all sorts of green things. Some we are confident can happen, others are totally dependent on people coming together to build the Festival as its own Commons.


**Here are the key ideas - please, let us know your thoughts!**